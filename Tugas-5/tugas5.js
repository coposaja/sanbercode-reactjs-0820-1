// Soal 1
function halo() {
  return 'Halo Sanbers!'
}

console.log(halo());

// Soal 2
function kalikan(a, b) {
  return a * b;
}

var num1 = 12
var num2 = 4

var hasilKali = kalikan(num1, num2)
console.log(hasilKali) // 48

// Soal 3
function introduce(name, age, address, hobby) {
  return "Nama saya ".concat(name, ", umur saya ", age, " tahun, alamat saya di ", address, ", dan saya punya hobby yaitu ", hobby, "!");
}

var name = "John"
var age = 30
var address = "Jalan belum jadi"
var hobby = "Gaming"

var perkenalan = introduce(name, age, address, hobby)
console.log(perkenalan) // Menampilkan "Nama saya John, umur saya 30 tahun, alamat saya di jalan belum jadi, dan saya punya hobby yaitu Gaming!" 

// Soal 4
var arrayDaftarPeserta = ["Asep", "laki-laki", "baca buku", 1992]
var newObject = {
  nama: arrayDaftarPeserta[0],
  jenisKelamin: arrayDaftarPeserta[1],
  hobi: arrayDaftarPeserta[2],
  tahunLahir: arrayDaftarPeserta[3],
};

// Soal 5
var objects = [
  {
    nama: 'strawberry',
    warna: 'merah',
    adaBijinya: false,
    harga: 9000,
  },
  {
    nama: 'jeruk',
    warna: 'orany',
    adaBijinya: true,
    harga: 800,
  },
  {
    nama: 'Semangka',
    warna: 'Hijau & Merah',
    adaBijinya: true,
    harga: 1000,
  },
  {
    nama: 'Pisang',
    warna: 'Kuning',
    adaBijinya: false,
    harga: 500,
  },
]
console.log(objects[0]);

// Soal 6
var dataFilm = []
function addFilm(array, film) {
  array.push({
    nama: film.nama,
    durasi: film.durasi,
    genre: film.genre,
    tahun: film.tahun
  })
}

addFilm(dataFilm, {
  nama: 'Film A',
  durasi: 120,
  genre: 'Horror',
  tahun: 2020
});

addFilm(dataFilm, {
  nama: 'Film A',
  durasi: 120,
  genre: 'Horror',
  tahun: 2020
});
