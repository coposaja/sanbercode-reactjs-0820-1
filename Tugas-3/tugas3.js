//Soal 1
var kataPertama = "saya";
var kataKedua = "senang";
var kataKetiga = "belajar";
var kataKeempat = "javascript";

kataKedua = kataKedua.charAt(0).toUpperCase() + kataKedua.substr(1);
console.log(kataPertama.concat(" ", kataKedua, " ", kataKetiga, " ", kataKeempat.toUpperCase()));

//Soal 2
var kataPertama = "1";
var kataKedua = "2";
var kataKetiga = "4";
var kataKeempat = "5";

console.log(parseInt(kataPertama) + parseInt(kataKedua) + parseInt(kataKetiga) + parseInt(kataKeempat));

//Soal 3
var kalimat = 'wah javascript itu keren sekali';

var kataPertama = kalimat.substring(0, 3);
var kataKedua = kalimat.substring(4, 14); // do your own! 
var kataKetiga = kalimat.substring(15, 18); // do your own! 
var kataKeempat = kalimat.substring(19, 24); // do your own! 
var kataKelima = kalimat.substr(25); // do your own! 

console.log('Kata Pertama: ' + kataPertama);
console.log('Kata Kedua: ' + kataKedua);
console.log('Kata Ketiga: ' + kataKetiga);
console.log('Kata Keempat: ' + kataKeempat);
console.log('Kata Kelima: ' + kataKelima);

//Soal 4
var nilai = 75;
if (nilai < 50) console.log('E');
else if (nilai < 60) console.log('D');
else if (nilai < 70) console.log('C');
else if (nilai < 80) console.log('B');
else console.log('A');

//Soal 5
var tanggal = 1;
var bulan = 3;
var tahun = 1998;

switch (bulan) {
  case 1:
    bulan = 'Januari';
    break;
  case 2:
    bulan = 'Februari';
    break
  case 3:
    bulan = 'Maret';
    break
  case 4:
    bulan = 'April';
    break
  case 5:
    bulan = 'Mei';
    break
  case 6:
    bulan = 'Juni';
    break
  case 7:
    bulan = 'Juli';
    break
  case 8:
    bulan = 'Agustus';
    break
  case 9:
    bulan = 'September';
    break
  case 10:
    bulan = 'Oktober';
    break
  case 11:
    bulan = 'November';
    break
  case 12:
    bulan = 'Desember';
    break
  default:
    break
}
console.log(tanggal.toString().concat(" ", bulan, " ", tahun));