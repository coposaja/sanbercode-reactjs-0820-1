var readBooksPromise = require('./promise.js')

var books = [
  { name: 'LOTR', timeSpent: 3000 },
  { name: 'Fidas', timeSpent: 2000 },
  { name: 'Kalkulus', timeSpent: 4000 }
]

// Lanjutkan code untuk menjalankan function readBooksPromise 
readBooksPromise(10000, books[0])
  .then((timeSpan) => readBooksPromise(timeSpan, books[1]))
  .then((timeSpan) => readBooksPromise(timeSpan, books[2]))
  .then((timeSpan) => console.log('Buku habis sisa', timeSpan))