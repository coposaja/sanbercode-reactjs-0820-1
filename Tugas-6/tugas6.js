// Soal 1
let circleArea = radius => { return Math.PI * radius * radius };
const triangleArea = (base, height) => { return base * height / 2; }

console.log(circleArea(7));
console.log(triangleArea(4, 8));

// Soal 2
let kalimat = ""
const addWord = (word) => {
  kalimat = `${kalimat} ${word}`
}

addWord('saya');
addWord('adalah');
addWord('seorang');
addWord('frontend');
addWord('developer');

console.log(kalimat);

// Soal 3
const newFunction = function literal(firstName, lastName) {
  return {
    firstName,
    lastName,
    fullName: function () {
      console.log(firstName + " " + lastName)
      return
    }
  }
}

//Driver Code 
newFunction("William", "Imoh").fullName()

// Soal 4
const newObject = {
  firstName: "Harry",
  lastName: "Potter Holt",
  destination: "Hogwarts React Conf",
  occupation: "Deve-wizard Avocado",
  spell: "Vimulus Renderus!!!"
}

const { firstName, lastName, destination, occupation } = newObject;

// Driver code
console.log(firstName, lastName, destination, occupation)

//Soal 5
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
const combined = [...west, ...east];
//Driver Code
console.log(combined)